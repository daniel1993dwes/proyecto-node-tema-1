"use strict";
const modelo = require("./modelo");
const http = require("http");

let atenderPaticion = (request, response) => {
  let responseCerrado = false;
  if (request.method === "GET") {
    if (request.url.startsWith("/Partidos")) {
      if (request.url === "/Partidos") {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.write(JSON.stringify(listarPartidos()));
      }
      else if (request.url === "/Partidos_importantes") {
        response.write(JSON.stringify(partidosImportantes()));
      }
      else if (request.url.startsWith("/Partidos_equipo")) {
        let parteURL = request.url.split("/");
        if (!isNaN(parteURL[2])) {
          let resultado = partidosGolesSeleccion(parteURL[2], parteURL[3]);
          if (resultado.partidos) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          } else {
            response.writeHead(404, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          }
        } else {
          let resultado = partidosEquipo(parteURL[2]);
          if (resultado.partidos) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          } else {
            response.writeHead(404, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          }
        }
      } else {
        response.writeHead(404, { "Content-Type": "text/plain" });
        response.write("Error al procesar petición");
      }
    } else if (request.url.startsWith("/Partido/")) {
      let parteURL = request.url.split("/");
      let resultado = buscarPorId(parteURL[2]);
      if (resultado.partido) {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.write(JSON.stringify(resultado));
      } else {
        response.writeHead(404, { "Content-Type": "text/plain" });
        response.write(JSON.stringify(resultado));
      }
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Error al procesar petición");
    }
  } else if (request.method === "POST") {
    if (request.url === "/Partido") {
      responseCerrado = true;
      let body = [];
      request
        .on("data", chunk => {
          body.push(chunk);
        })
        .on("end", () => {
          body = Buffer.concat(body).toString();
          let resultado = anyadirPartido(body);
          if (resultado.error == false) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          } else {
            response.writeHead(404, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          }
          response.end();
        });
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Error al procesar petición");
    }
  } else if (request.method === "PUT") {
    if (request.url === "/Partido") {
      responseCerrado = true;
      let body = [];
      request
        .on("data", chunk => {
          body.push(chunk);
        })
        .on("end", () => {
          body = Buffer.concat(body).toString();
          let resultado = modificarPartido(body);
          if (resultado.error == false) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          } else {
            response.writeHead(404, { "Content-Type": "text/plain" });
            response.write(JSON.stringify(resultado));
          }
          response.end();
        });
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write("Error al procesar petición");
    }
  } else if (
    request.method === "DELETE" &&
    request.url.startsWith("/Partido")
  ) {
    let parteURL = request.url.split("/");
    responseCerrado = true;
    let resultado = borrarPartido(parteURL[2]);
    if (resultado.error == false) {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.write(JSON.stringify(resultado));
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.write(JSON.stringify(resultado));
    }
    response.end();
  } else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.write("Error al procesar petición");
  }
  if (!responseCerrado) response.end();
};

let borrarPartido = COD_ID => {
  let resultado = {};
  if (modelo.partidos.borrarPartido(COD_ID)) {
    resultado.error = false;
    resultado.errorMensaje = "";
  } else {
    resultado.error = true;
    resultado.errorMensaje = "No existe un partido con esa id";
  }
  return resultado;
};

let modificarPartido = body => {
  let partido = JSON.parse(body);
  let resultado = {};
  if (
    modelo.partidos.modificarPartido(
      partido.COD_ID,
      partido.EQUIPO_L,
      partido.EQUIPO_V,
      partido.FECHA,
      partido.SEDE,
      partido.RESULTADO_L,
      partido.RESULTADO_V,
      partido.ASISTENCIA
    )
  ) {
    resultado.error = false;
    resultado.errorMensaje = "";
  } else {
    resultado.error = true;
    resultado.errorMensaje =
      "Ya existe un partido con esa id o algun dato es erróneo";
  }
  return resultado;
};

let anyadirPartido = body => {
  let partido = JSON.parse(body);
  let resultado = {};
  if (
    modelo.partidos.nuevoPartido(
      partido.COD_ID,
      partido.EQUIPO_L,
      partido.EQUIPO_V,
      partido.FECHA,
      partido.SEDE,
      partido.RESULTADO_L,
      partido.RESULTADO_V,
      partido.ASISTENCIA
    )
  ) {
    resultado.error = false;
    resultado.errorMensaje = "";
  } else {
    resultado.error = true;
    resultado.errorMensaje =
      "Ya existe un partido con esa id o algun dato es erróneo";
  }
  return resultado;
};
let partidosGolesSeleccion = (goles, seleccion) => {
  let resultado = {};
  let partidos = modelo.partidos.listarPartidosSeleccionGoles(seleccion, goles);
  if (partidos.length > 0) {
    resultado.error = false;
    resultado.errorMensaje = "";
    resultado.partidos = partidos;
  } else {
    resultado.error = true;
    resultado.errorMensaje =
      "No existe ningun partido con esa selección y esos goles";
  }
  return resultado;
};

let partidosEquipo = seleccion => {
  let resultado = {};
  let partidos = modelo.partidos.listarPartidosSeleccion(seleccion);
  if (partidos.length > 0) {
    resultado.error = false;
    resultado.errorMensaje = "";
    resultado.partidos = partidos;
  } else {
    resultado.error = true;
    resultado.errorMensaje = "No existe ningun partido con esa selección";
  }
  return resultado;
};

let partidosImportantes = () => {
  let partidos = modelo.partidos.listarPartidosImportantes();
  return partidos;
};

let buscarPorId = COD_ID => {
  let resultado = {};
  let partido = modelo.partidos.buscarPartidoPorId(COD_ID);
  if (partido) {
    resultado.error = false;
    resultado.errorMensaje = "";
    resultado.partido = partido;
  } else {
    resultado.error = true;
    resultado.errorMensaje = "No se ha encontrado ningún partido";
  }
  return resultado;
};

let listarPartidos = () => {
  return modelo.partidos.listarPartidos();
};

http.createServer(atenderPaticion).listen(8080);
