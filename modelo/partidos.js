"use strict";
const fs = require("fs");
const fichero = "partidos.json";
const fichero_de_borrados = 'partidosBorrados.json';
const moment = require("moment");
const eventos_config = require('./eventos_config.js');
const Emitter = require('events');
let gestor_eventos = new Emitter();

let cargarPartidos = () => {
  try {
    return JSON.parse(fs.readFileSync(fichero, "utf8"));
  } catch (e) {
    return [];
  }
};

let guardarPartidos = partidos => {
  fs.writeFileSync(fichero, JSON.stringify(partidos));
};

let buscarPartidoPorId = COD_ID => {
  let partidos = cargarPartidos();
  let resultado = partidos.filter(partido => partido.COD_ID == COD_ID);
  if (resultado.length > 0) return resultado[0];
  else return undefined;
};

let nuevoPartido = (
  COD_ID,
  EQUIPO_L,
  EQUIPO_V,
  FECHA,
  SEDE,
  RESULTADO_L,
  RESULTADO_V,
  ASISTENCIA
) => {
  if (
    !buscarPartidoPorId(COD_ID) &&
    validaElementos(
      COD_ID,
      EQUIPO_L,
      EQUIPO_V,
      FECHA,
      SEDE,
      +RESULTADO_L,
      +RESULTADO_V,
      +ASISTENCIA
    )
  ) {
    let partidos = cargarPartidos();
    let partido = {
      COD_ID: COD_ID,
      EQUIPO_L: EQUIPO_L,
      EQUIPO_V: EQUIPO_V,
      FECHA: FECHA,
      SEDE: SEDE,
      RESULTADO_L: RESULTADO_L,
      RESULTADO_V: RESULTADO_V,
      ASISTENCIA: ASISTENCIA
    };
    partidos.push(partido);
    guardarPartidos(partidos);
    return true;
  } else return false;
};

let validaElementos = (
  COD_ID,
  EQUIPO_L,
  EQUIPO_V,
  FECHA,
  SEDE,
  RESULTADO_L,
  RESULTADO_V,
  ASISTENCIA
) => {
  let fechaActual = new moment();
  let fechaPartido = new moment(FECHA, "DD/MM/YYYY hh:mm");

  if (
    COD_ID != "" &&
    COD_ID != undefined &&
    EQUIPO_L != "" &&
    EQUIPO_L != undefined &&
    EQUIPO_V != "" &&
    EQUIPO_V != undefined &&
    FECHA != "" &&
    FECHA != undefined &&
    SEDE != "" &&
    SEDE != undefined &&
    RESULTADO_L != "" &&
    RESULTADO_L != undefined &&
    RESULTADO_L >= 0 &&
    RESULTADO_V != "" &&
    RESULTADO_V != undefined &&
    RESULTADO_V >= 0 &&
    ASISTENCIA != "" &&
    ASISTENCIA != undefined &&
    ASISTENCIA >= 0 &&
    fechaPartido.isValid()
  ) {
    let contiene = fechaActual.from(fechaPartido);
    if (contiene.indexOf("in") != -1) return true;
  } else return false;
};

let borrarPartido = COD_ID => {
  let partidos = cargarPartidos();
  gestor_eventos.emit(eventos_config.eventos.HECHO, COD_ID);
  let partidosFiltrados = partidos.filter(partido => partido.COD_ID != COD_ID);
  if (partidosFiltrados.length !== partidos.length) guardarPartidos(partidosFiltrados);
  return partidosFiltrados.length !== partidos.length;
};

let modificarPartido = (
  COD_ID,
  EQUIPO_L,
  EQUIPO_V,
  FECHA,
  SEDE,
  RESULTADO_L,
  RESULTADO_V,
  ASISTENCIA
) => {
  if (
    buscarPartidoPorId(COD_ID) &&
    borrarPartido(COD_ID) &&
    validaElementos(
      COD_ID,
      EQUIPO_L,
      EQUIPO_V,
      FECHA,
      SEDE,
      +RESULTADO_L,
      +RESULTADO_V,
      +ASISTENCIA
    )
  ) {
    let partidos = cargarPartidos();
    let partido = {
      COD_ID: COD_ID,
      EQUIPO_L: EQUIPO_L,
      EQUIPO_V: EQUIPO_V,
      FECHA: FECHA,
      SEDE: SEDE,
      RESULTADO_L: RESULTADO_L,
      RESULTADO_V: RESULTADO_V,
      ASISTENCIA: ASISTENCIA
    };
    partidos.push(partido);
    guardarPartidos(partidos);
    return true;
  } else return false;
};

let listarPartidosImportantes = () => {
  let partidos = cargarPartidos();
  let retorna = partidos.filter(partido => {
    let fechaPartido = new moment(partido.FECHA, "DD/MM/YYYY hh:mm");
    if (fechaPartido.month() + 1 == 7) return partido;
  });
  return retorna;
};

let listarPartidosSeleccion = seleccion => {
  let partidos = cargarPartidos();
  let retorna = partidos.filter(partido => {
    if (
      partido.EQUIPO_L.toLowerCase() == seleccion.toLowerCase() ||
      partido.EQUIPO_V.toLowerCase() == seleccion.toLowerCase()
    ) {
      return partido;
    }
  });
  return retorna;
};

let listarPartidosSeleccionGoles = (seleccion, goles) => {
  let partidos = listarPartidosSeleccion(seleccion);
  let retorna = partidos.filter(partido => {
    let TOTAL = +partido.RESULTADO_L + +partido.RESULTADO_V;
    if (TOTAL >= goles) {
      return partido;
    }
  });
  return retorna;
};

let guardarBorrado = (COD_ID) => {
  let partido = buscarPartidoPorId(COD_ID);
  fs.appendFileSync(fichero_de_borrados, JSON.stringify(partido))
};
gestor_eventos.on(eventos_config.eventos.HECHO, guardarBorrado);

module.exports = {
  listarPartidos: cargarPartidos,
  buscarPartidoPorId: buscarPartidoPorId,
  nuevoPartido: nuevoPartido,
  borrarPartido: borrarPartido,
  modificarPartido: modificarPartido,
  listarPartidosImportantes: listarPartidosImportantes,
  listarPartidosSeleccion: listarPartidosSeleccion,
  listarPartidosSeleccionGoles: listarPartidosSeleccionGoles
};
